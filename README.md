Dependencies
============

To run

* python2
* python-xlib
* python-imaging (or python-pil)
* python-imaging-tk (or python-pil.imagetk)
* python-pygame

To test

* python-coverage2

Copyright Notices
=================

Font
----

Lets Go Digital Regular - Made by WLM Fonts, provided under the Ubuntu Font
License 1.0 (also included in this project)

Alarm Sound
-----------

Alarm Clock's alarm on - Made by PlayPauseAndRewind, licensed under the
Creative Commons 0 license. (included in this project)
