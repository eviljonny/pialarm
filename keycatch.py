#!/usr/bin/env python

import apps
import alarm
import atexit
import code
import datetime
import random
import soundLoader
import Tkinter
import Xlib.display as display
import Xlib.X as X

from PIL import Image, ImageTk

GPIO_AVAILABLE = True
RFLIB_AVAILABLE = True
RFLIB_SEND_PIN = 7

try:
    try:
        import rflib
    except ImportError:
        RFLIB_AVAILABLE = False

    import RPi.GPIO as GPIO
except ImportError:
    GPIO_AVAILABLE = False

BG_COLOUR = '#1b191a'

BUTTONS = {
    1: 23,
    2: 22,
    3: 27,
    4: 18,
}
CHANNELS_TO_BUTTONS = {v:k for k,v in BUTTONS.iteritems()}

# RF_CODES[group][button][on|off]
RF_CODES = {
    1: {
        1: { "on": 1381717, "off": 1381716 },
        2: { "on": 1394005, "off": 1394004 },
        3: { "on": 1397077, "off": 1397076 },
        4: { "on": 1397845, "off": 1397844 },
    },
    2: {
        1: { "on": 4527445, "off": 4527444 },
        2: { "on": 4539733, "off": 4539732 },
        3: { "on": 4542805, "off": 4542804 },
        4: { "on": 4543573, "off": 4543572 },
    },
    3: {
        1: { "on": 5313877, "off": 5313876 },
        2: { "on": 5326165, "off": 5326164 },
        3: { "on": 5329237, "off": 5329236 },
        4: { "on": 5330005, "off": 5330004 },
    },
    4: {
        1: { "on": 5510485, "off": 5510484 },
        2: { "on": 5522773, "off": 5522772 },
        3: { "on": 5525845, "off": 5525844 },
        4: { "on": 5526613, "off": 5526613 },
    },
}

@atexit.register
def autorepeat():
    d = display.Display()

    d.change_keyboard_control(auto_repeat_mode=X.AutoRepeatModeOn)

    d.get_keyboard_control()

class Gui(Tkinter.Frame):
    GEOMETRY = "320x240+100+100"

    def __init__(self, parent, fullscreen=False):
        Tkinter.Frame.__init__(self, parent)

        self.parent = parent

        self._initialise_frame(fullscreen)
        self._soundLoader = soundLoader.SoundLoader()

        self._problem_generator = code.ProblemGenerator(random)
        self._alarmKeeper = alarm.AlarmKeeper(clock=datetime)
        self._alarmKeeper.load_alarms()

        self._welcome_app = apps.WelcomeApp(
            parent, "images/headings/welcome.png"
        )
        self._alarm_clock_app = apps.AlarmClockApp(
            parent, "images/headings/alarm-clock.png"
        )
        self._socket_control_app = apps.SocketControlApp(
            parent, "images/headings/socketctl.png", self._socket_button_pressed
        )
        self._mood_mon_app = apps.MoodMonApp(
            parent, "images/headings/mood-mon.png"
        )

        self._view_alarms_app = apps.ViewAlarmsApp(
            parent, "images/headings/alarm-clock.png", self._alarmKeeper, self.alarm_adder_app
        )

        self._alarm_adder_app = apps.AlarmAdderApp(
            parent, "images/headings/alarm-clock.png", self._alarmKeeper, self.view_alarms_app
        )

        self._header = apps.Header(self.parent, self._welcome_app.header_image)

        self._alarming = False
        self._content = None
        self._alarm_sound = None

        self._setup_gpio()

        self.parent.protocol("WM_DELETE_WINDOW", self.quit)

        self._set_content(self._welcome_app)

        self.parent.after(1000, self._timer_tick)

    def _initialise_frame(self, fullscreen):
        self.parent.configure(bg=BG_COLOUR)
        self.parent.geometry(self.GEOMETRY)

        self._set_background("images/small_bg.png")

        self._fullscreen = True
        self._set_fullscreen_state()
        self.steal_focus()
        self._bind_key_events()

    def _set_fullscreen_state(self):
        self.parent.attributes("-fullscreen", self._fullscreen)
        self.parent.geometry(self.GEOMETRY)

    def steal_focus(self, event=None):
        self.parent.lift()
        self.parent.focus_force()

    def _timer_tick(self, *args, **kwargs):
        if self._alarmKeeper.check_alarms(datetime.datetime.now()):
            self.trigger_alarm_clock()

        self.parent.after(1000, self._timer_tick)

    def trigger_alarm_clock(self):
        self.alarm_clock_app(None)

    def _play_alarm(self):
        alarm_sound = self._soundLoader.loadSound(("sounds", "alarm_clocks_alarm_on.ogg"))
        self._alarm_sound = alarm_sound.play(-1)

    def turn_on_socket(self, group, socket):
        self._set_plug_state(group, socket, "on")

    def turn_off_socket(self, group, socket):
        self._set_plug_state(group, socket, "off")

    def _set_plug_state(self, group, socket, state):
        if RFLIB_AVAILABLE:
            rflib.sendcode(RFLIB_SEND_PIN, RF_CODES[group][socket][state])
        else:
            print "Sending code (%s) for group: %s socket: %s state: %s" % (
                RF_CODES[group][socket][state], group, socket, state
            )

    def _setup_gpio(self):
        if GPIO_AVAILABLE:
            GPIO.setmode(GPIO.BCM)

            for gpio_pin in BUTTONS.values():
                GPIO.setup(gpio_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
                GPIO.add_event_detect(
                    gpio_pin,
                    GPIO.FALLING,
                    callback=self._channel_signal,
                    bouncetime=500,
                )

    def _channel_signal(self, channel):
        print "CHANNEL SIGNAL RECEVIED ON %s" % channel

        if not self._alarming:
            if CHANNELS_TO_BUTTONS[channel] == 1:
                self.view_alarms_app(None)
            elif CHANNELS_TO_BUTTONS[channel] == 2:
                self.socket_control_app(None)
            elif CHANNELS_TO_BUTTONS[channel] == 3:
                self.mood_mon_app(None)
            elif CHANNELS_TO_BUTTONS[channel] == 4:
                self.quit(None)

    def _set_background(self, image_path):
        im = Image.open(image_path)
        self.tkimage = ImageTk.PhotoImage(im)
        image_label = Tkinter.Label(self.parent, image=self.tkimage, bd=0)
        image_label.place(x=0, y=0, anchor=Tkinter.NW)

    def _bind_key_events(self):
        self.parent.bind("<Escape>", self.toggle_fullscreen)
        self.parent.bind("<q>", self.quit)
        self.parent.bind("<m>", self.mood_mon_app)
        self.parent.bind("<a>", self.alarm_clock_app)
        self.parent.bind("<w>", self.welcome_app)
        self.parent.bind("<s>", self.socket_control_app)
        self.parent.bind("<v>", self.view_alarms_app)

        for num in xrange(10):
            num_str = "%s" % num
            kp_str = "<KP_%s>" % num
            self.parent.bind(num_str, self.num)
            self.parent.bind(kp_str, self.kp_num)

    def kp_num(self, event):
        """ Keypad numbers keysym are all KP_x where x is the keypad number
        """
        self._number_pressed(event.keysym[-1], event)

    def num(self, event):
        self._number_pressed(event.keysym, event)

    def _number_pressed(self, num, event):
        if self._alarming:
            done = self._validator.next_digit(num)
            if done:
                self.alarm_cancel()

    def quit(self, event=None):
        if not self._alarming:
            if GPIO_AVAILABLE:
                GPIO.cleanup()
            self.parent.destroy()

            self.turn_off_socket(2,1)

    def toggle_fullscreen(self, event):
        self._fullscreen = not self._fullscreen
        self._set_fullscreen_state()

    def _stop_alarm(self):
        self._alarm_sound.stop()
        self._alarm_sound = None

    def alarm_cancel(self):
        self._stop_alarm()
        self._alarming = False
        self.welcome_app(None)

    def _set_content(self, content):
        self._header.set_image(content.header_image)

        if self._content:
            self._content.hide()
            self._content = None

        self._content = content
        self._content.show()

    def welcome_app(self, event):
        if not self._alarming:
            self._set_content(self._welcome_app)

    def alarm_clock_app(self, event):
        if not self._alarming:
            (problem1, problem2), validator = self._problem_generator.create_problem()
            self._validator = validator

            self._alarming = True

            self._alarm_clock_app.set_problems(problem1, problem2)

            self._set_content(self._alarm_clock_app)

            self.turn_on_socket(2, 1)

            self.steal_focus()
            self._play_alarm()

    def socket_control_app(self, event):
        if not self._alarming:
            self._set_content(self._socket_control_app)

    def mood_mon_app(self, event):
        if not self._alarming:
            self._set_content(self._mood_mon_app)

    def view_alarms_app(self, event=None):
        if not self._alarming:
            self._set_content(self._view_alarms_app)

    def alarm_adder_app(self, event=None):
        if not self._alarming:
            self._set_content(self._alarm_adder_app)

    def _socket_button_pressed(self, band, socket, command):
        if not self._alarming:
            self._set_plug_state(band, socket, command)

if __name__ == "__main__":
    d = display.Display()
    d.change_keyboard_control(auto_repeat_mode=X.AutoRepeatModeOff)
    d.get_keyboard_control()

    root = Tkinter.Tk()

    gui = Gui(root)

    root.mainloop()

