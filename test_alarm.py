#!/usr/bin/env python

import alarm
import datetime
import os
import tempfile
import unittest

class FakeDateTime(datetime.datetime):
    fake_now = None

    @classmethod
    def set_fake_now(cls, fake_now):
        cls.fake_now = fake_now

    @classmethod
    def clear_fake_now(cls):
        cls.fake_now = None

    @classmethod
    def now(cls):
        return cls.fake_now or datetime.datetime.now()

class FakeDate(datetime.date):
    fake_today = None

    @classmethod
    def set_fake_today(cls, fake_today):
        cls.fake_today = fake_today

    @classmethod
    def clear_fake_today(cls):
        cls.fake_today = None

    @classmethod
    def today(cls):
        return cls.fake_today or datetime.date.today()

class FakeTime(datetime.time):
    pass

class FakeClockModule(object):
    timedelta = datetime.timedelta
    datetime = FakeDateTime
    date = FakeDate
    time = FakeTime

# 2000-01-13 - Is a thursday
TEST_BASE_DATETIME = FakeDateTime(2000,1,13,12,0)
NOT_ALARM_DAY = [
    alarm.MON,
    alarm.TUE,
    alarm.WED,
    alarm.FRI,
    alarm.SAT,
    alarm.SUN,
]
NOT_ALARM_DAY_TOMORROW = [
    alarm.MON,
    alarm.TUE,
    alarm.WED,
    alarm.THUR,
    alarm.SAT,
    alarm.SUN,
]
NOT_ALARM_TODAY_TOMORROW = [
    alarm.MON,
    alarm.TUE,
    alarm.WED,
    alarm.SAT,
    alarm.SUN,
]
NOT_ALARM_DAY_YESTERDAY = [
    alarm.MON,
    alarm.TUE,
    alarm.THUR,
    alarm.FRI,
    alarm.SAT,
    alarm.SUN,
]
NOT_ALARM_DAY_YESTERDAY_TODAY = [
    alarm.MON,
    alarm.TUE,
    alarm.FRI,
    alarm.SAT,
    alarm.SUN,
]
NOT_ALARM_DAY_TWODAYSAGO = [
    alarm.MON,
    alarm.WED,
    alarm.THUR,
    alarm.FRI,
    alarm.SAT,
    alarm.SUN,
]
NOT_ALARM_DAY_TWODAYSAGO_YESTERDAY = [
    alarm.MON,
    alarm.THUR,
    alarm.FRI,
    alarm.SAT,
    alarm.SUN,
]
NOT_ALARM_DAY_TWODAYSAGO_YESTERDAY_TODAY = [
    alarm.MON,
    alarm.FRI,
    alarm.SAT,
    alarm.SUN,
]


class TestAlarmKeeper(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestAlarmKeeper, self).__init__(*args, **kwargs)

        tmp_file = tempfile.NamedTemporaryFile(prefix='alarmkeeper_tmp_')
        self._tmp_file = tmp_file.name
        tmp_file.close()

    def __del__(self):
        if os.path.isfile(self._tmp_file):
            os.remove(self._tmp_file)

        if hasattr(super(self.__class__, self), "__del__"):
            super(self.__class__, self).__del__()

    def tearDown(self):
        self.ak = None
        FakeDateTime.clear_fake_now()
        FakeDate.clear_fake_today()

    def setUp(self):
        FakeDateTime.set_fake_now(TEST_BASE_DATETIME)
        FakeDate.set_fake_today(TEST_BASE_DATETIME.date())
        self.ak = alarm.AlarmKeeper(
            clock=FakeClockModule,
            save_file_name=self._tmp_file
        )

    def test_set_alarm(self):
        self.ak.set_alarm(hour=12, minute=57, days=alarm.WEEKENDS)
        self.assertEqual(len(self.ak.alarms), 1)
        self.assertEqual(
            self.ak.alarms[0].alarm_time,
            FakeTime(12, 57)
        )
        self.assertEqual(
            self.ak.alarms[0].days,
            alarm.WEEKENDS
        )

    def test_set_two_alarms(self):
        self.ak.set_alarm(hour=12, minute=57, days=alarm.WEEKENDS)
        self.ak.set_alarm(hour=06, minute=15, days=alarm.WEEKDAYS)

        self.assertEqual(len(self.ak.alarms), 2)
        self.assertEqual(
            self.ak.alarms[0].alarm_time,
            FakeTime(12, 57)
        )
        self.assertEqual(
            self.ak.alarms[0].days,
            alarm.WEEKENDS
        )
        self.assertEqual(
            self.ak.alarms[1].alarm_time,
            FakeTime(06, 15)
        )
        self.assertEqual(
            self.ak.alarms[1].days,
            alarm.WEEKDAYS
        )

    def test_check_no_alarms_set(self):
        self.assertFalse(
            self.ak.check_alarms(
                FakeDateTime.now(),
            )
        )

    def test_check_alarms_no_alarms_activated(self):
        self.ak.set_alarm(hour=12, minute=57, days=alarm.EVERYDAY)

        self.assertFalse(
            self.ak.check_alarms(FakeDateTime.now())
        )

    def test_check_alarms_one_activated(self):
        self.ak.set_alarm(hour=11, minute=13, days=alarm.EVERYDAY)
        self.ak.set_alarm(hour=12, minute=13, days=alarm.EVERYDAY)

        self.assertEqual(
            self.ak.alarms[0]._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 13
            )
        )
        self.assertEqual(
            self.ak.alarms[1]._next_alarm,
            FakeDateTime(
                2000, 1, 13, 12, 13
            )
        )

        self.assertTrue(
            self.ak.check_alarms(FakeDateTime(2000,1,13,12,13))
        )

        self.assertEqual(
            self.ak.alarms[0]._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 13
            )
        )
        self.assertEqual(
            self.ak.alarms[1]._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 13
            )
        )

    def test_check_alarms_two_activated(self):
        self.ak.set_alarm(hour=11, minute=13, days=alarm.EVERYDAY)
        self.ak.set_alarm(hour=12, minute=13, days=alarm.EVERYDAY)

        self.assertEqual(
            self.ak.alarms[0]._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 13
            )
        )
        self.assertEqual(
            self.ak.alarms[1]._next_alarm,
            FakeDateTime(
                2000, 1, 13, 12, 13
            )
        )

        self.assertTrue(
            self.ak.check_alarms(FakeDateTime(2000,1,14,11,15))
        )

        self.assertEqual(
            self.ak.alarms[0]._next_alarm,
            FakeDateTime(
                2000, 1, 15, 11, 13
            )
        )
        self.assertEqual(
            self.ak.alarms[1]._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 13
            )
        )

    def test_check_alarms_one_expiring(self):
        self.ak.set_alarm(hour=15, minute=13, days=alarm.EVERYDAY)
        self.ak.set_alarm(
            hour=11, minute=13, days=alarm.ONCE,
            date=FakeDate(2000,1,14)
        )
        self.ak.set_alarm(hour=12, minute=13, days=alarm.EVERYDAY)

        self.assertEqual(
            self.ak.alarms[0]._next_alarm,
            FakeDateTime(
                2000, 1, 13, 15, 13
            )
        )
        self.assertEqual(
            self.ak.alarms[1]._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 13
            )
        )
        self.assertEqual(
            self.ak.alarms[2]._next_alarm,
            FakeDateTime(
                2000, 1, 13, 12, 13
            )
        )

        self.assertTrue(
            self.ak.check_alarms(FakeDateTime(2000,1,14,11,15))
        )

        # Check one alarm has been deleted
        self.assertEqual(len(self.ak.alarms), 2)

        self.assertEqual(
            self.ak.alarms[0]._next_alarm,
            FakeDateTime(
                2000, 1, 14, 15, 13
            )
        )
        self.assertEqual(
            self.ak.alarms[1]._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 13
            )
        )

    def test_check_alarms_two_expiring(self):
        self.ak.set_alarm(
            hour=15, minute=13, days=alarm.ONCE,
            date=FakeDate(2000,1,13)
        )
        self.ak.set_alarm(
            hour=11, minute=13, days=alarm.ONCE,
            date=FakeDate(2000,1,14)
        )
        self.ak.set_alarm(hour=12, minute=13, days=alarm.EVERYDAY)

        self.assertEqual(
            self.ak.alarms[0]._next_alarm,
            FakeDateTime(
                2000, 1, 13, 15, 13
            )
        )
        self.assertEqual(
            self.ak.alarms[1]._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 13
            )
        )
        self.assertEqual(
            self.ak.alarms[2]._next_alarm,
            FakeDateTime(
                2000, 1, 13, 12, 13
            )
        )

        self.assertTrue(
            self.ak.check_alarms(FakeDateTime(2000,1,14,11,15))
        )

        # Check one alarm has been deleted
        self.assertEqual(len(self.ak.alarms), 1)

        self.assertEqual(
            self.ak.alarms[0]._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 13
            )
        )

    def test_check_alarms_all_expiring(self):
        self.ak.set_alarm(
            hour=15, minute=13, days=alarm.ONCE,
            date=FakeDate(2000,1,13)
        )
        self.ak.set_alarm(
            hour=11, minute=13, days=alarm.ONCE,
            date=FakeDate(2000,1,14)
        )
        self.ak.set_alarm(
            hour=12, minute=13, days=alarm.ONCE,
            date=FakeDate(2000,1,14)
        )

        self.assertEqual(
            self.ak.alarms[0]._next_alarm,
            FakeDateTime(
                2000, 1, 13, 15, 13
            )
        )
        self.assertEqual(
            self.ak.alarms[1]._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 13
            )
        )
        self.assertEqual(
            self.ak.alarms[2]._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 13
            )
        )

        self.assertTrue(
            self.ak.check_alarms(FakeDateTime(2000,1,14,14,15))
        )

        # Check one alarm has been deleted
        self.assertEqual(len(self.ak.alarms), 0)


class TestDTFromDandT(unittest.TestCase):
    """ Tests for the dt_from_d_and_t method
    """
    def test_dt_from_d_and_t(self):
        test_alarm = alarm.Alarm(hour=0, minute=0, days=alarm.EVERYDAY, clock=FakeClockModule)
        date = FakeDate(2007, 7, 10)
        time = FakeTime(21, 05)

        result = test_alarm.dt_from_d_and_t(date, time)

        self.assertIsInstance(result, FakeDateTime)
        self.assertEqual(result.year, 2007)
        self.assertEqual(result.month, 7)
        self.assertEqual(result.day, 10)
        self.assertEqual(result.hour, 21)
        self.assertEqual(result.minute, 5)

class TestAlarm(unittest.TestCase):
    """ Other tests for alarm
    """
    def tearDown(self):
        FakeDateTime.clear_fake_now()
        FakeDate.clear_fake_today()

    def setUp(self):
        FakeDateTime.set_fake_now(TEST_BASE_DATETIME)
        FakeDate.set_fake_today(TEST_BASE_DATETIME.date())

    def test_advance_alarm_to_next_alarmday(self):
        test_alarm = alarm.Alarm(
            20, 45,
            alarm.WEEKDAYS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,13)
        )

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 13, 20, 45
            )
        )

        test_alarm.advance_alarm_date()

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 20, 45
            )
        )

    def test_advance_alarm_to_non_alarmday(self):
        test_alarm = alarm.Alarm(
            20, 45,
            NOT_ALARM_DAY_TOMORROW,
            clock=FakeClockModule,
            date=FakeDate(2000,1,13)
        )

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 13, 20, 45
            )
        )

        test_alarm.advance_alarm_date()

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 15, 20, 45
            )
        )

    def test_advance_once_alarm(self):
        test_alarm = alarm.Alarm(
            20, 45,
            alarm.ONCE,
            clock=FakeClockModule,
            date=FakeDate(2000,1,13)
        )

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 13, 20, 45
            )
        )

        self.assertRaises(
            alarm.AlarmExpired,
            test_alarm.advance_alarm_date
        )

    def test_alarm_activation_before_alarm(self):
        test_alarm = alarm.Alarm(
            20, 45,
            alarm.ONCE,
            clock=FakeClockModule,
            date=FakeDate(2000,1,13)
        )

        self.assertFalse(
            test_alarm.check_for_activation(FakeDateTime(2000,1,13,20,44))
        )

    def test_alarm_activation_exactly_alarm_datetime(self):
        test_alarm = alarm.Alarm(
            20, 45,
            alarm.ONCE,
            clock=FakeClockModule,
            date=FakeDate(2000,1,13)
        )

        self.assertTrue(
            test_alarm.check_for_activation(FakeDateTime(2000,1,13,20,45))
        )

    def test_alarm_activation_after_alarm(self):
        test_alarm = alarm.Alarm(
            20, 45,
            alarm.ONCE,
            clock=FakeClockModule,
            date=FakeDate(2000,1,13)
        )

        self.assertTrue(
            test_alarm.check_for_activation(FakeDateTime(2000,1,13,20,45))
        )


class TestAlarmInitialisation(unittest.TestCase):
    """ Tests for Alarm initialisation
    """

    def tearDown(self):
        FakeDateTime.clear_fake_now()
        FakeDate.clear_fake_today()

    def setUp(self):
        FakeDateTime.set_fake_now(TEST_BASE_DATETIME)
        FakeDate.set_fake_today(TEST_BASE_DATETIME.date())

    """ The full list of tests in this class appear below
    """
    # Recurring
        # without date specified
            # today is an alarm day
                # with a time earlier than now - alarm is set for next day
                # with a time exactly now - alarm is set for next day
                # with a time after now - alarm is set for later today
            # today is not an alarm day
                # with a time earlier than now - alarm is set for next day
                # with a time exactly now - alarm is set for next day
                # with a time after now - alarm is set for next day
            # today and tomorrow are not alarm days
                # with a time earlier than now - alarm is set for two days time
                # with a time exactly now - alarm is set for two days time
                # with a time after now - alarm is set for two days time
        # with date specified
            # tomorrow
                # which is an alarm day
                    # with a time earlier than now - alarm is set for next day
                    # with a time exactly now - alarm is set for next day
                    # with a time after now - alarm is set for later today
                # which is not an alarm day
                    # with a time earlier than now - alarm is set for next day
                    # with a time exactly now - alarm is set for next day
                    # with a time after now - alarm is set for later today
            # today
                # which is an alarm day
                    # with a time earlier than now - alarm is set for next day
                    # with a time exactly now - alarm is set for next day
                    # with a time after now - alarm is set for later today
                # which is not an alarm day
                    # with a time earlier than now - alarm is set for next day
                    # with a time exactly now - alarm is set for next day
                    # with a time after now - alarm is set for later today
            # yesterday
                # which is an alarm day
                    # with a time earlier than now - alarm is set for next day
                    # with a time exactly now - alarm is set for next day
                    # with a time after now - alarm is set for later today
                # which is not an alarm day
                    # with a time earlier than now - alarm is set for next day
                    # with a time exactly now - alarm is set for next day
                    # with a time after now - alarm is set for later today
                # which is not an alarm day, neither is yesterday
                    # with a time earlier than now - alarm is set for next day
                    # with a time exactly now - alarm is set for next day
                    # with a time after now - alarm is set for later today
            # two days ago
                # which is an alarm day
                    # with a time earlier than now - alarm is set for next day
                    # with a time exactly now - alarm is set for next day
                    # with a time after now - alarm is set for later today
                # which is not an alarm day
                    # with a time earlier than now - alarm is set for next day
                    # with a time exactly now - alarm is set for next day
                    # with a time after now - alarm is set for later today
                # which is not an alarm day, neither is the day after
                    # with a time earlier than now - alarm is set for next day
                    # with a time exactly now - alarm is set for next day
                    # with a time after now - alarm is set for later today
                # which is not an alarm day, neither is the day after or today
                    # with a time earlier than now - alarm is set for next day
                    # with a time exactly now - alarm is set for next day
                    # with a time after now - alarm is set for later today
    # Once
        # without date specified - Error
        # with date specified
            # Date is before today
                # time is earlier than now
                # time is exactly now
                # time is later than now
            # Date is today
                # time is earlier than now
                # time is exactly now
                # time is later than now
            # Date is tomorrow
                # time is earlier than now
                # time is exactly now
                # time is later than now

    """ Now the tests
    """
    # Recurring
        # without date specified
            # today is an alarm day
                # with a time earlier than now - alarm is set for next day
    def test_recur_nodate_alarmday_earlier(self):
        test_alarm = alarm.Alarm(10, 0, alarm.WEEKDAYS, clock=FakeClockModule)

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 10, 0
            )
        )

                # with a time exactly now - alarm is set for next day
    def test_recur_nodate_alarmday_now(self):
        test_alarm = alarm.Alarm(12, 0, alarm.WEEKDAYS, clock=FakeClockModule)

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 0
            )
        )

                # with a time after now - alarm is set for later today
    def test_recur_nodate_alarmday_later(self):
        test_alarm = alarm.Alarm(13, 0, alarm.WEEKDAYS, clock=FakeClockModule)

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 13, 13, 0
            )
        )

            # today is not an alarm day
                # with a time earlier than now - alarm is set for next day
    def test_recur_nodate_notalarmday_earlier(self):
        test_alarm = alarm.Alarm(11, 0, NOT_ALARM_DAY, clock=FakeClockModule)

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 0
            )
        )

                # with a time exactly now - alarm is set for next day
    def test_recur_nodate_notalarmday_now(self):
        test_alarm = alarm.Alarm(12, 0, NOT_ALARM_DAY, clock=FakeClockModule)

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 0
            )
        )

                # with a time after now - alarm is set for next day
    def test_recur_nodate_notalarmday_later(self):
        test_alarm = alarm.Alarm(12, 01, NOT_ALARM_DAY, clock=FakeClockModule)

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 01
            )
        )


            # today and tomorrow are not alarm days
                # with a time earlier than now - alarm is set for two days time
    def test_recur_nodate_notalarmdaytodaytomorrow_earlier(self):
        test_alarm = alarm.Alarm(11, 59, NOT_ALARM_TODAY_TOMORROW, clock=FakeClockModule)

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 15, 11, 59
            )
        )


                # with a time exactly now - alarm is set for two days time
    def test_recur_nodate_notalarmdaytodaytomorrow_now(self):
        test_alarm = alarm.Alarm(12, 0, NOT_ALARM_TODAY_TOMORROW, clock=FakeClockModule)

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 15, 12, 0
            )
        )


                # with a time after now - alarm is set for two days time
    def test_recur_nodate_notalarmdaytodaytomorrow_later(self):
        test_alarm = alarm.Alarm(12, 01, NOT_ALARM_TODAY_TOMORROW, clock=FakeClockModule)

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 15, 12, 01
            )
        )
        # with date specified
            # tomorrow
                # which is an alarm day
                    # with a time earlier than now - alarm is set for next day
    def test_recur_date_tomorrow_alarmday_earlier(self):
        test_alarm = alarm.Alarm(
            11, 59,
            alarm.WEEKDAYS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,14)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 59
            )
        )

                    # with a time exactly now - alarm is set for next day
    def test_recur_date_tomorrow_alarmday_now(self):
        test_alarm = alarm.Alarm(
            12, 00,
            alarm.WEEKDAYS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,14)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 00
            )
        )

                    # with a time after now - alarm is set for later today
    def test_recur_date_tomorrow_alarmday_later(self):
        test_alarm = alarm.Alarm(
            12, 01,
            alarm.WEEKDAYS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,14)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 01
            )
        )

                # which is not an alarm day
                    # with a time earlier than now - alarm is set for next day
    def test_recur_date_tomorrow_notalarmday_earlier(self):
        test_alarm = alarm.Alarm(
            11, 59,
            alarm.WEEKENDS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,14)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 15, 11, 59
            )
        )

                    # with a time exactly now - alarm is set for next day
    def test_recur_date_tomorrow_notalarmday_now(self):
        test_alarm = alarm.Alarm(
            12, 00,
            alarm.WEEKENDS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,14)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 15, 12, 00
            )
        )


                    # with a time after now - alarm is set for later today
    def test_recur_date_tomorrow_notalarmday_later(self):
        test_alarm = alarm.Alarm(
            12, 01,
            alarm.WEEKENDS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,14)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 15, 12, 01
            )
        )


            # today
                # which is an alarm day
                    # with a time earlier than now - alarm is set for next day
    def test_recur_date_today_alarmday_earlier(self):
        test_alarm = alarm.Alarm(
            11, 59,
            alarm.WEEKDAYS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,13)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 59
            )
        )


                    # with a time exactly now - alarm is set for next day
    def test_recur_date_today_alarmday_now(self):
        test_alarm = alarm.Alarm(
            12, 00,
            alarm.WEEKDAYS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,13)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 00
            )
        )


                    # with a time after now - alarm is set for later today
    def test_recur_date_today_alarmday_later(self):

        test_alarm = alarm.Alarm(
            12, 01,
            alarm.WEEKDAYS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,13)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 13, 12, 01
            )
        )


                # which is not an alarm day
                    # with a time earlier than now - alarm is set for next day
    def test_recur_date_today_notalarmday_earlier(self):
        test_alarm = alarm.Alarm(
            11, 59,
            NOT_ALARM_DAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,13)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 59
            )
        )

                    # with a time exactly now - alarm is set for next day
    def test_recur_date_today_notalarmday_now(self):
        test_alarm = alarm.Alarm(
            12, 00,
            NOT_ALARM_DAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,13)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 00
            )
        )

                    # with a time after now - alarm is set for later today
    def test_recur_date_today_notalarmday_later(self):
        test_alarm = alarm.Alarm(
            12, 01,
            NOT_ALARM_DAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,13)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 01
            )
        )


            # yesterday
                # which is an alarm day
                    # with a time earlier than now - alarm is set for next day
    def test_recur_date_yesterday_alarmday_earlier(self):
        test_alarm = alarm.Alarm(
            11, 59,
            alarm.WEEKDAYS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,12)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 59
            )
        )

                    # with a time exactly now - alarm is set for next day
    def test_recur_date_yesterday_alarmday_now(self):
        test_alarm = alarm.Alarm(
            12, 00,
            alarm.WEEKDAYS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,12)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 00
            )
        )

                    # with a time after now - alarm is set for later today
    def test_recur_date_yesterday_alarmday_later(self):
        test_alarm = alarm.Alarm(
            12, 01,
            alarm.WEEKDAYS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,12)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 13, 12, 01
            )
        )

                # which is not an alarm day
                    # with a time earlier than now - alarm is set for next day
    def test_recur_date_yesterday_notalarmday_earlier(self):
        test_alarm = alarm.Alarm(
            11, 59,
            NOT_ALARM_DAY_YESTERDAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,12)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 59
            )
        )

                    # with a time exactly now - alarm is set for next day
    def test_recur_date_yesterday_notalarmday_now(self):
        test_alarm = alarm.Alarm(
            12, 00,
            NOT_ALARM_DAY_YESTERDAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,12)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 00
            )
        )

                    # with a time after now - alarm is set for later today
    def test_recur_date_yesterday_notalarmday_later(self):
        test_alarm = alarm.Alarm(
            12, 01,
            NOT_ALARM_DAY_YESTERDAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,12)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 13, 12, 01
            )
        )


                # which is not an alarm day, neither is today
                    # with a time earlier than now - alarm is set for next day
    def test_recur_date_yesterday_notalarmdayortoday_earlier(self):
        test_alarm = alarm.Alarm(
            11, 59,
            NOT_ALARM_DAY_YESTERDAY_TODAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,12)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 59
            )
        )

                    # with a time exactly now - alarm is set for next day
    def test_recur_date_yesterday_notalarmdayortoday_now(self):
        test_alarm = alarm.Alarm(
            12, 00,
            NOT_ALARM_DAY_YESTERDAY_TODAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,12)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 00
            )
        )


                    # with a time after now - alarm is set for later today
    def test_recur_date_yesterday_notalarmdayortoday_later(self):
        test_alarm = alarm.Alarm(
            12, 01,
            NOT_ALARM_DAY_YESTERDAY_TODAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,12)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 01
            )
        )


            # two days ago
                # which is an alarm day
                    # with a time earlier than now - alarm is set for next day
    def test_recur_date_twodaysago_alarmday_earlier(self):
        test_alarm = alarm.Alarm(
            11, 59,
            alarm.WEEKDAYS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 59
            )
        )

                    # with a time exactly now - alarm is set for next day
    def test_recur_date_twodaysago_alarmday_now(self):
        test_alarm = alarm.Alarm(
            12, 00,
            alarm.WEEKDAYS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 00
            )
        )

                    # with a time after now - alarm is set for later today
    def test_recur_date_twodaysago_alarmday_later(self):
        test_alarm = alarm.Alarm(
            12, 01,
            alarm.WEEKDAYS,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 13, 12, 01
            )
        )
                # which is not an alarm day
                    # with a time earlier than now - alarm is set for next day
    def test_recur_date_twodaysago_notalarmday_earlier(self):
        test_alarm = alarm.Alarm(
            11, 59,
            NOT_ALARM_DAY_TWODAYSAGO,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 59
            )
        )

                    # with a time exactly now - alarm is set for next day
    def test_recur_date_twodaysago_notalarmday_now(self):
        test_alarm = alarm.Alarm(
            12, 00,
            NOT_ALARM_DAY_TWODAYSAGO,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 00
            )
        )

                    # with a time after now - alarm is set for later today
    def test_recur_date_twodaysago_notalarmday_later(self):
        test_alarm = alarm.Alarm(
            12, 01,
            NOT_ALARM_DAY_TWODAYSAGO,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 13, 12, 01
            )
        )


                # which is not an alarm day, neither is the day after
                    # with a time earlier than now - alarm is set for next day
    def test_recur_date_twodaysago_notalarmdayoryesterday_earlier(self):
        test_alarm = alarm.Alarm(
            11, 59,
            NOT_ALARM_DAY_TWODAYSAGO_YESTERDAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 59
            )
        )


                    # with a time exactly now - alarm is set for next day
    def test_recur_date_twodaysago_notalarmdayoryesterday_now(self):
        test_alarm = alarm.Alarm(
            12, 00,
            NOT_ALARM_DAY_TWODAYSAGO_YESTERDAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 00
            )
        )


                    # with a time after now - alarm is set for later today
    def test_recur_date_twodaysago_notalarmdayoryesterday_later(self):
        test_alarm = alarm.Alarm(
            12, 01,
            NOT_ALARM_DAY_TWODAYSAGO_YESTERDAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 13, 12, 01
            )
        )


                # which is not an alarm day, neither is the day after or today
                    # with a time earlier than now - alarm is set for next day
    def test_recur_date_twodaysago_notalarmdayoryesterdayortoday_earlier(self):
        test_alarm = alarm.Alarm(
            11, 59,
            NOT_ALARM_DAY_TWODAYSAGO_YESTERDAY_TODAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 59
            )
        )


                    # with a time exactly now - alarm is set for next day
    def test_recur_date_twodaysago_notalarmdayoryesterdayortoday_now(self):
        test_alarm = alarm.Alarm(
            12, 00,
            NOT_ALARM_DAY_TWODAYSAGO_YESTERDAY_TODAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 00
            )
        )


                    # with a time after now - alarm is set for later today
    def test_recur_date_twodaysago_notalarmdayoryesterdayortoday_later(self):
        test_alarm = alarm.Alarm(
            12, 01,
            NOT_ALARM_DAY_TWODAYSAGO_YESTERDAY_TODAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 01
            )
        )

                # which is an alarm day, as is yesterday, but today isn't
                    # with a time earlier than now - alarm is set for tomorrow
    def test_recur_date_twodaysago_alarmdayandyesterdaynottoday_earlier(self):
        test_alarm = alarm.Alarm(
            11, 59,
            NOT_ALARM_DAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 59
            )
        )


                    # with a time exactly now - alarm is set for tomorrow
    def test_recur_date_twodaysago_alarmdayandyesterdaynottoday_now(self):
        test_alarm = alarm.Alarm(
            12, 00,
            NOT_ALARM_DAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 00
            )
        )


                    # with a time after now - alarm is set for tomorrow
    def test_recur_date_twodaysago_alarmdayandyesterdaynottoday_later(self):
        test_alarm = alarm.Alarm(
            12, 01,
            NOT_ALARM_DAY,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 01
            )
        )

                # which is an alarm day, as is yesterday, today is, but tomorrow isn't
                    # with a time earlier than now - alarm is set for 2 days time
    def test_recur_date_twodaysago_alarmdaynottomorrow_earlier(self):
        test_alarm = alarm.Alarm(
            11, 59,
            NOT_ALARM_DAY_TOMORROW,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 15, 11, 59
            )
        )

                    # with a time exactly now - alarm is set for 2 days time
    def test_recur_date_twodaysago_alarmdaynottomorrow_now(self):
        test_alarm = alarm.Alarm(
            12, 00,
            NOT_ALARM_DAY_TOMORROW,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 15, 12, 00
            )
        )


                    # with a time after now - alarm is set for today
    def test_recur_date_twodaysago_alarmdaynottomorrow_later(self):
        test_alarm = alarm.Alarm(
            12, 01,
            NOT_ALARM_DAY_TOMORROW,
            clock=FakeClockModule,
            date=FakeDate(2000,1,11)
        )

        self.assertEqual (
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 13, 12, 01
            )
        )


    # Once
        # without date specified - Error
    def test_once_nodate(self):
        self.assertRaises(
            ValueError,
            alarm.Alarm,
            20, 10, alarm.ONCE,
            clock=FakeClockModule,
        )
        # with date specified
            # Date is before today
                # time is earlier than now
    def test_once_date_yesterday_earlier(self):
        self.assertRaises(
            ValueError,
            alarm.Alarm,
            10, 0, alarm.ONCE, date=FakeDate(2000,1,12),
            clock=FakeClockModule,
        )
                # time is exactly now
    def test_once_date_yesterday_now(self):
        self.assertRaises(
            ValueError,
            alarm.Alarm,
            12, 0, alarm.ONCE, date=FakeDate(2000,1,12),
            clock=FakeClockModule,
        )

                # time is later than now
    def test_once_date_yesterday_later(self):
        self.assertRaises(
            ValueError,
            alarm.Alarm,
            14, 0, alarm.ONCE, date=FakeDate(2000,1,12),
            clock=FakeClockModule,
        )

            # Date is today
                # time is earlier than now
    def test_once_date_today_earlier(self):
        self.assertRaises(
            ValueError,
            alarm.Alarm,
            11, 0, alarm.ONCE, date=FakeDate(2000,1,13),
            clock=FakeClockModule,
        )

                # time is exactly now
    def test_once_date_today_now(self):
        self.assertRaises(
            ValueError,
            alarm.Alarm,
            11, 0, alarm.ONCE, date=FakeDate(2000,1,13),
            clock=FakeClockModule,
        )


                # time is later than now
    def test_once_date_today_later(self):
        test_alarm = alarm.Alarm(
            14, 0, alarm.ONCE, date=FakeDate(2000,1,13),
            clock=FakeClockModule,
        )

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 13, 14, 0
            )
        )

            # Date is tomorrow
                # time is earlier than now
    def test_once_date_tomorrow_earlier(self):
        test_alarm = alarm.Alarm(
            11, 59, alarm.ONCE, date=FakeDate(2000,1,14),
            clock=FakeClockModule,
        )

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 11, 59
            )
        )

                # time is exactly now
    def test_once_date_tomorrow_now(self):
        test_alarm = alarm.Alarm(
            12, 0, alarm.ONCE, date=FakeDate(2000,1,14),
            clock=FakeClockModule,
        )

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 0
            )
        )

                # time is later than now
    def test_once_date_tomorrow_later(self):
        test_alarm = alarm.Alarm(
            12, 01, alarm.ONCE, date=FakeDate(2000,1,14),
            clock=FakeClockModule,
        )

        self.assertEqual(
            test_alarm._next_alarm,
            FakeDateTime(
                2000, 1, 14, 12, 01
            )
        )

if __name__ == "__main__":
    unittest.main()

