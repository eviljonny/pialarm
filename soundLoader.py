import os
import pygame

class SoundLoader(object):
    def __init__(self):
        super(SoundLoader, self).__init__()

        pygame.mixer.init()

        self.__sounds = {}

    def loadSound(self, pathParts):
        path = os.path.join(*pathParts)

        if path in self.__sounds:
            self.__sounds[path]["refcount"] += 1
        else:
            self.__sounds[path] = {
                "refcount": 1,
                "sound": pygame.mixer.Sound(path)
            }

        return self.__sounds[path]["sound"]

    def unloadSound(self, pathParts):
        path = os.path.join(*pathParts)

        if path not in self.__sounds:
            raise ValueError("Sound not found to unload: %r" % path)

        self.__sounds[path]["refcount"] -= 1

        if self.__sounds[path]["refcount"] == 0:
            del self.__sounds[path]
