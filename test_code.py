#!/usr/bin/env python

import unittest

import code

class FakeRandomiser():
    """ A class to mock the random functions that the problem
        generator needs so we can return fixed numbers and
        operators
    """
    def __init__(self, number_sequence, first_op, second_op):
        number_sequence.reverse()

        self._sequence = number_sequence
        self.set_next_two_operators(first_op, second_op)

    def randint(self, start, end):
        """ Gets the next int in the sequence
        """
        next_num = self._sequence.pop()

        if next_num < start or next_num > end:
            raise ValueError(
                "Fake randomiser asked for a value with " \
                "a range excluding the next in the fake sequence"
            )

        return next_num

    def set_next_two_operators(self, first, second):
        """ Set what the next two operators should be.
        """
        self._first_op = first
        self._second_op = second

    def choice(self, sequence):
        """ Return the first and then second operators.
        """
        if self._first_op is None and self._second_op is None:
            raise ValueError(
                "Fake randomiser asked to make a choice when the " \
                "next operator has not yet been set"
            )

        if self._first_op is not None:
            op = self._first_op
            self._first_op = None
        else:
            op = self._second_op
            self._second_op = None

        return op


class TestProblemGenerator(unittest.TestCase):
    """ Tests for the problem generator
    """
    def _init_generator(self, sequence, op1, op2):
        random = FakeRandomiser(sequence, op1, op2)

        pg = code.ProblemGenerator(random)
        return pg.create_problem()

    def _assert_correct(self, p1, p1_ans, p2, p2_ans, v, code):
        self.assertEqual(p1, p1_ans)
        self.assertEqual(p2, p2_ans)
        self.assertEqual(v._code, code)

    def test_simple_addition(self):
        (p1, p2), v = self._init_generator([5,5,50,49], "+", "+")

        self._assert_correct(
            p1, "5 + 5",
            p2, "50 + 49",
            v, "1099"
        )

    def test_simple_subtraction(self):
        (p1, p2), v = self._init_generator([99, 1, 11, 1], "-", "-")

        self._assert_correct(
            p1, "99 - 1",
            p2, "11 - 1",
            v, "9810"
        )

    def test_reversing_subtraction(self):
        (p1, p2), v = self._init_generator([1, 99, 1, 11], "-", "-")

        self._assert_correct(
            p1, "99 - 1",
            p2, "11 - 1",
            v, "9810"
        )

    def test_small_result_subtraction(self):
        (p1, p2), v = self._init_generator([11, 1, 11, 2], "-", "-")

        self._assert_correct(
            p1, "11 - 1",
            p2, "21 - 2",
            v, "1019"
        )

    def test_small_reversing_subtraction(self):
        (p1, p2), v = self._init_generator([1, 11, 2, 11], "-", "-")

        self._assert_correct(
            p1, "11 - 1",
            p2, "21 - 2",
            v, "1019"
        )

    def test_simple_multi(self):
        (p1, p2), v = self._init_generator([2, 5, 10, 9], "*", "*")

        self._assert_correct(
            p1, "2 * 5",
            p2, "10 * 9",
            v, "1090"
        )

    def test_too_low_multi(self):
        (p1, p2), v = self._init_generator([2, 2, 2, 3, 2, 4, 2, 5, 3, 3, 2, 10], "*", "*")

        self._assert_correct(
            p1, "2 * 5",
            p2, "2 * 10",
            v, "1020"
        )

    def test_too_high_mult(self):
        (p1, p2), v = self._init_generator([10, 10, 7, 3, 10, 10, 10, 10, 5, 8], "*", "*")

        self._assert_correct(
            p1, "7 * 3",
            p2, "5 * 8",
            v, "2140"
        )

    def test_unknown_operator(self):
        random = FakeRandomiser([1,2,3,4], "//", "*")

        pg = code.ProblemGenerator(random)

        self.assertRaises(
            ValueError,
            pg.create_problem,
        )

    def test_leading_zeros(self):
        random = FakeRandomiser([1,2,3,4], "//", "*")

        pg = code.ProblemGenerator(random)

        def create_fake_generator(num1, op, num2):
            def _fake_generate_problem():
                return num1.pop(), op.pop(), num2.pop()

            return _fake_generate_problem

        pg._generate_problem = create_fake_generator([2,2], ["*", "+"], [3,2])


        (p1, p2), v = pg.create_problem()

        self.assertEqual(p1, "2 + 2")
        self.assertEqual(p2, "2 * 3")
        self.assertEqual(v._code, "0406")


class TestCode(unittest.TestCase):
    """ Tests for the Code verifier class.
    """

    def test_success_code(self):
        test_code = code.Code("1234")

        self.assertFalse(test_code.next_digit(1))
        self.assertFalse(test_code.next_digit(2))
        self.assertFalse(test_code.next_digit(3))
        self.assertTrue(test_code.next_digit(4))

    def test_failed_code(self):
        test_code = code.Code("1234")

        self.assertFalse(test_code.next_digit(1))
        self.assertFalse(test_code.next_digit(2))
        self.assertFalse(test_code.next_digit(3))
        self.assertFalse(test_code.next_digit(5))

    def test_fail_then_ok(self):
        test_code = code.Code("1234")

        self.assertFalse(test_code.next_digit(1))
        self.assertFalse(test_code.next_digit(2))
        self.assertFalse(test_code.next_digit(3))
        self.assertFalse(test_code.next_digit(5))
        self.assertFalse(test_code.next_digit(1))
        self.assertFalse(test_code.next_digit(2))
        self.assertFalse(test_code.next_digit(3))
        self.assertTrue(test_code.next_digit(4))

    def test_single_digit_fail_then_ok(self):
        test_code = code.Code("1234")

        self.assertFalse(test_code.next_digit(5))
        self.assertFalse(test_code.next_digit(1))
        self.assertFalse(test_code.next_digit(2))
        self.assertFalse(test_code.next_digit(3))
        self.assertTrue(test_code.next_digit(4))

    def test_code_too_long(self):
        self.assertRaises(
            ValueError,
            code.Code,
            "12345678900"
        )

    def test_code_too_short(self):
        self.assertRaises(
            ValueError,
            code.Code,
            "123"
        )

    def test_too_many_digits(self):
        test_code = code.Code("1234")

        self.assertRaises(
            ValueError,
            test_code.next_digit,
            "12"
        )

    def test_code_not_a_string(self):
        self.assertRaises(
            ValueError,
            code.Code,
            1234
        )

    def test_code_with_leading_zero(self):
        test_code = code.Code("0123")

        self.assertFalse(test_code.next_digit(0))
        self.assertFalse(test_code.next_digit(1))
        self.assertFalse(test_code.next_digit(2))
        self.assertTrue(test_code.next_digit(3))

        self.assertFalse(test_code.next_digit(1))
        self.assertFalse(test_code.next_digit(2))
        self.assertFalse(test_code.next_digit(3))


if __name__ == "__main__":
    unittest.main()

