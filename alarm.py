""" The AlarmKeeper class manages alarms for you, if you call check_for_alarm it will tell you whether
an alarm needs to trigger either now, or since the last time the alarm keeper was triggered today

NOTE: All times are 24 hour format e.g. 00:00 - 23:59

example:

    import alarm

    ak = alarm.AlarmKeeper(clock=datetime)
    # Set an alarm for 06:15 on weekdays
    ak.set_alarm(hour=6, minute=15, days=alarm.WEEKDAYS)

    # Set an alarm to trigger once only at 18:45
    ak.set_alarm(hour=18, minute=45, days=alarm.ONCE)

    # Set an alarm to trigger at 00:00 on Saturdays
    ak.set_alarm(hour=0, minute=0, days=[alarm.SAT])

    # Set an alarm to trigger at 23:59 every Wednesday and Friday
    ak.set_alarm(hour=23, minute=59, days=[alarm.WED, alarm.FRI])
"""

import functools
import os
import pickle

DEFAULT_ALARM_FILE = os.path.expanduser(
    os.path.join("~",".pialarms")
)

# Day numbers reflect date.weekday() values
MON = 0
TUE = 1
WED = 2
THUR = 3
FRI = 4
SAT = 5
SUN = 6

INDEX_TO_DAYS = {
    0: "Mon",
    1: "Tue",
    2: "Wed",
    3: "Thur",
    4: "Fri",
    5: "Sat",
    6: "Sun",
}

WEEKDAYS = [MON, TUE, WED, THUR, FRI]
WEEKENDS = [SAT, SUN]
EVERYDAY = WEEKDAYS + WEEKENDS
ONCE = None


class AlarmExpired(StandardError):
    pass


class AlarmKeeper(object):
    def __init__(self, save_file_name=DEFAULT_ALARM_FILE, clock=None, *args, **kwargs):
        super(AlarmKeeper, self).__init__(*args, **kwargs)

        if clock is None:
            raise ValueError("You must pass a clock module in to use")

        self.clock = clock

        self.alarm_file = save_file_name

        self.alarms = []

    def delete_alarm(self, alarm_to_delete):
        try:
            self.alarms.remove(alarm_to_delete)
            self._auto_save()
            return True
        except ValueError:
            return False

    def set_alarm(self, hour, minute, days, date=None):
        self.alarms.append(Alarm(hour, minute, days, date, clock=self.clock))
        self._auto_save()

    def _auto_save(self):
        self.save_alarms()

    def check_alarms(self, check_datetime):
        """ Check for alarm activations at check_datetime.
            Any alarms triggered will be advanced.
            Any alarms that expired will be deleted

            Returns:
                True or False specifying whether an alarm activated
        """

        alarm_activated = False

        for i, alarm in enumerate(self.alarms):
            if alarm.check_for_activation(check_datetime):
                alarm_activated = True
                try:
                    alarm.advance_alarm_date(check_datetime)
                except AlarmExpired:
                    # Mark alarm for deletion
                    self.alarms[i] = None

        # Filter out the dead alarms
        if alarm_activated:
            self.alarms = [alarm for alarm in self.alarms if alarm]
            self._auto_save()

        return alarm_activated

    def save_alarms(self):
        """ Saves the alarms to file filname.

            Returns:
                True on success, an Exception object if an exception occured.
                Test with `save_alarms is True`
        """

        # pickle to filename.tmp
        # if successful move filename.tmp over filename
        tmp_filename = self.alarm_file + ".tmp"

        with open(tmp_filename, "w") as temp_file:
            try:
                pickle.dump(self.alarms, temp_file)
            except Exception as e:
                print "EXCEPTION DUMPING %s" % e
                return e

        try:
            os.rename(tmp_filename, self.alarm_file)
        except Exception as e:
            print "EXCEPTION MOVING %s" % e
            return e

        return True

    def load_alarms(self):
        """ Loads alarms from file filename

            Returns:
                True on success, an Exception object if an exception occured.
                Test with `load_alarms is True`
        """
        try:
            with open(self.alarm_file, "r") as alarmfile:
                try:
                    self.alarms = pickle.load(alarmfile)
                except Exception as e:
                    print "EXCEPTION LOADING %s" % e
                    return e
        except IOError as e:
            # If it's a file not found error ignore it and carry on
            if e.errno != 2:
                raise e

        for loaded_alarm in self.alarms:
            loaded_alarm.set_clock(self.clock)

        return True


@functools.total_ordering
class Alarm(object):
    def __init__(self, hour, minute, days, date=None, clock=None):
        if days == ONCE and date is None:
            raise ValueError("For non recurring alarms you must specify a date")

        if clock is None:
            raise ValueError("You must pass a clock module in to use")

        self.clock = clock

        self.alarm_time = self.clock.time(hour, minute)
        self.days = days

        # Set first alarm
        self._next_alarm = self._set_first_alarm(date=date)

        self._previous_alarm = None

    def __eq__(self, other):
        return (
            self.alarm_time == other.alarm_time
            and self.days == other.days
            and self._next_alarm == other._next_alarm
        )

    def __lt__(self, other):
        return self.alarm_time < other.alarm_time

    def __getstate__(self):
        return {
            "time": self.alarm_time,
            "prev": self._previous_alarm,
            "next": self._next_alarm,
            "days": self.days,
        }

    def __setstate__(self, state):
        self.alarm_time = state['time']
        self._previous_alarm = state['prev']
        self._next_alarm = state['next']
        self.days = state['days']

    def set_clock(self, clock):
        self.clock = clock

    def dt_from_d_and_t(self, date, time):
        return self.clock.datetime(
            date.year, date.month, date.day,
            time.hour, time.minute
        )

    def get_days_label(self):
        if self.days is ONCE:
            return "Once"
        elif self.days == WEEKDAYS:
            return "Weekdays"
        elif self.days == WEEKENDS:
            return "Weekends"
        elif self.days == EVERYDAY:
            return "Everyday"
        else:
            day_string = ""

            for day in self.days:
                day_string += INDEX_TO_DAYS[day] + " "

            return day_string.strip()

    def check_for_activation(self, checkDateTime):
        """ Check if the alarm activates at checkDateTime.
        """
        if checkDateTime >= self._next_alarm:
            return True

        return False

    def advance_alarm_date(self, now=None):
        """ Advance the alarm to the next activation.

            Raises:
                alarm.AlarmExpired if the alarm has expired (e.g. past end date or was a ONCE only)
        """
        if self.days == ONCE:
            raise AlarmExpired("One use alarm expired")

        now = now or self.clock.datetime.now()

        self._previous_alarm = self._next_alarm
        self._next_alarm = self._get_next_alarm_date(startDate=self._previous_alarm, now=now)

    def _get_next_alarm_date(self, startDate=None, now=None):
        """ Get the date for the next alarm (excluding today)
        """
        if self.days is ONCE:
            raise AlarmExpired("Alarm has expired")

        now = now or self.clock.datetime.now()

        test_date = (startDate or self.clock.date.today()) + self.clock.timedelta(days=1)
        test_datetime = self.dt_from_d_and_t(test_date, self.alarm_time)

        while test_date.weekday() not in self.days or test_datetime <= now:
            test_date = test_date + self.clock.timedelta(days=1)
            test_datetime = self.dt_from_d_and_t(test_date, self.alarm_time)

        return test_date

    def _set_first_alarm(self, date=None):
        now = self.clock.datetime.now()

        # If a specific date has been set for the first alarm use it
        if date is not None:
            # Schedule for next alarm date after (and including) the specified
            if self.days is ONCE:
                alarm_date = date
            # All the remaining are recurring alarms
            # If today isn't an alarm date
            elif date.weekday() not in self.days:
                alarm_date = self._get_next_alarm_date(startDate=date)
            # If date is today, and the time is earlier than now
            elif date == now.date() and self.alarm_time <= now.time():
                alarm_date = self._get_next_alarm_date()
            # If alarm date is in the past
            elif date < now.date():
                alarm_date = self._get_next_alarm_date(startDate=date)
            else:
                alarm_date = date

            alarm_date_time = self.dt_from_d_and_t(alarm_date, self.alarm_time)

            # If the alarm date was in the past raise an error
            if alarm_date_time <= now:
                raise ValueError("Alarms can't be set in the past")

            return alarm_date_time

        # If today is an alarm day and the time of the alarm is later than now
        if now.weekday() in self.days and now.time() < self.alarm_time:
            # schedule for today
            return self.dt_from_d_and_t(now.date(), self.alarm_time)
        else:
            # schedule for next alarm day
            alarm_date = self._get_next_alarm_date()
            return self.dt_from_d_and_t(alarm_date, self.alarm_time)


