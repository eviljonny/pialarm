""" code module for generating and validating security codes

    example usage:

    >>> import code
    >>> problems, validator = create_problem

    >>> print problems # So the answer is 3645
    ("12 * 3", "57 - 12")

    >>> validator.next_digit("3")
    False
    >>> validator.next_digit("6")
    False
    >>> validator.next_digit("4")
    False
    >>> validator.next_digit("5")
    True

"""

class ProblemGenerator(object):
    operations = {
        "-": lambda a,b: a - b,
        "+": lambda a,b: a + b,
        "*": lambda a,b: a * b,
    }

    def __init__(self, engine):
        """ Args:
                engine: Something which implements randint(start, end) and choice(list) methods
        """
        self._engine = engine

    def create_problem(self):
        """ Generate a simple maths problem to solve.
            Returns a tuple of strings which are the equations to solve and a Code object
            representing the answer.

            >>> code.create_problem()
            ("12 * 3", "57 - 12"), <code.Code object at 0x7f97f07f7111>
        """

        num1, op, num2 = self._generate_problem()
        problem1 = "%d %s %d" % (num1, op, num2)
        answer1 = self._get_answer(num1, op, num2)

        if answer1 < 10:
            leading_zero_1 = "0"
        else:
            leading_zero_1 = ""

        num1, op, num2 = self._generate_problem()
        problem2 = "%d %s %d" % (num1, op, num2)
        answer2 = self._get_answer(num1, op, num2)

        if answer2 < 10:
            leading_zero_2 = "0"
        else:
            leading_zero_2 = ""

        complete_answer = "%s%d%s%d" % (
            leading_zero_1, answer1,
            leading_zero_2, answer2
        )

        code_verifier = Code(complete_answer)

        return (problem1, problem2), code_verifier

    def _generate_problem(self):
        answer = 9999

        operation = self._engine.choice(self.operations.keys())

        while answer < 10 or answer >= 100:
            if operation == "*":
                num1 = self._engine.randint(2, 10)
                num2 = self._engine.randint(2, 10)

            elif operation == "-":
                num1 = self._engine.randint(1, 99)
                num2 = self._engine.randint(1, 99)

                if num1 - num2 < 0:
                    tmpnum = num1
                    num1 = num2
                    num2 = tmpnum

                if num1 - num2 < 10:
                    num1 += 10

            elif operation == "+":
                num1 = self._engine.randint(5, 50)
                num2 = self._engine.randint(5, 49)

            else:
                raise ValueError("Unknown operator")


            answer = self._get_answer(num1, operation, num2)

        return num1, operation, num2

    def _get_answer(self, num1, operator, num2):
        return self.operations[operator](num1, num2)


class Code(object):
    def __init__(self, code_string):
        if not isinstance(code_string, basestring):
            raise ValueError("Code must be a string")

        code_length = len(code_string)

        if code_length > 10:
            raise ValueError("Code must be no more than 10 digits")

        if code_length < 4:
            raise ValueError("Code must be at least 4 digits")

        self._code = code_string
        self._code_length = code_length
        self._position = 0
        self._code_buffer = [None for x in xrange(code_length)]

    def next_digit(self, digit):
        if not isinstance(digit, basestring):
            digit = str(digit)

        if len(digit) != 1:
            raise ValueError("You must provide a single digit")

        self._code_buffer[self._position] = digit
        self._position += 1

        if self._position >= self._code_length:
            self._position = 0

        return self.check_match()

    def check_match(self):
        for i in xrange(self._code_length):
            cur_pos = self._position + i

            if cur_pos >= self._code_length:
                cur_pos -= self._code_length

            if self._code[i] != self._code_buffer[cur_pos]:
               return False

        return True
