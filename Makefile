
clean:
	find . -name '*.pyc' -exec rm -f '{}' \;
	rm -rf htmlcov/
	rm -f .coverage

test:
	coverage2 run test_code.py && coverage2 run --append test_alarm.py && rm -rf htmlcov/ && coverage2 html
