import Tkinter
import ttk

from PIL import Image, ImageTk

BG_COLOUR = '#1b191a'

class Header(Tkinter.Frame):
    def __init__(self, parent, image):
        Tkinter.Frame.__init__(self, parent, bg=BG_COLOUR, bd=0)
        self.parent = parent
        self.place(width=273, height=60)

        self._image = image

        image_label = Tkinter.Label(self, image=image, bd=0)
        image_label.place(x=0, y=0, anchor=Tkinter.NW)

        self._image_label = image_label

    def set_image(self, image):
        self.image = image
        self._image_label.configure(image=image)

class Content(Tkinter.Frame):
    def __init__(self, parent, header_image_path):
        Tkinter.Frame.__init__(self, parent, bg=BG_COLOUR)
        self.parent = parent
        self._content = None

        tmpImage = Image.open(header_image_path)
        self.header_image = ImageTk.PhotoImage(tmpImage)

    def hide(self):
        self.place_forget()

    def show(self):
        self.place(x=0, y=60, width=320, height=180)

class WelcomeApp(Content):
    pass

class MoodMonApp(Content):
    pass

class AlarmClockApp(Content):
    def set_problems(self, problem1, problem2):
        problem_text = "%s\n%s" % (problem1, problem2)

        if self._content:
            self._content.place_forget()

        self._content = Tkinter.Label(
            self,
            text=problem_text,
            bd=0,
            bg=BG_COLOUR,
            fg="#00FF00",
            font="Mono 50 bold"
        )
        self._content.place(x=0, y=0, relwidth=1, relheight=1)

class SocketControlApp(Content):
    def __init__(self, parent, image, button_callback, *args, **kwargs):
        Content.__init__(self, parent, image, *args, **kwargs)

        self._bands = ["I", "II", "III", "IV"]
        self._band_buttons = []
        self._band_image_unselected = [None, None, None, None]
        self._band_image_selected = [None, None, None, None]
        self._device_label_images = [None, None, None, None]
        self._on_image = None
        self._off_image = None

        self._button_callback =  button_callback

        self.band = Tkinter.IntVar()

        self._init_main_frame()
        self._init_band_frame()
        self._init_control_frame()

    def _init_main_frame(self):
        self.configure(background = BG_COLOUR)

    def _init_band_frame(self):
        self._band_frame = Tkinter.Frame(self, bg=BG_COLOUR)
        self._band_frame.grid(row=0, column=0, pady=5)

        for i, band in enumerate(self._bands):
            tmpImage = Image.open("images/radiobuttons/" + band + "_unselected.png")
            self._band_image_unselected[i] =  ImageTk.PhotoImage(tmpImage)

            tmpImage = Image.open("images/radiobuttons/" + band + "_selected.png")
            self._band_image_selected[i] =  ImageTk.PhotoImage(tmpImage)

            button = Tkinter.Radiobutton(
                self._band_frame,
                text=band,
                variable=self.band,
                value=(i+1),
                indicatoron=0,
                image=self._band_image_unselected[i],
                selectimage=self._band_image_selected[i],
                relief=Tkinter.FLAT,
                offrelief=Tkinter.FLAT,
                overrelief=Tkinter.FLAT,
                borderwidth=0,
                highlightbackground=BG_COLOUR,
            )
            button.grid(row=i, column=0)
            self._band_buttons.append(button)

        self._band_buttons[1].select()

    def make_callback(self, button, command):
        def f():
            self._button_callback(self.get_band(), button , command)

        return f

    def _init_control_frame(self):
        self._control_frame = Tkinter.Frame(self, height=20, bg=BG_COLOUR)
        self._control_frame.grid(row=0, column=1, padx=15)

        tmpImage = Image.open("images/labels/on.png")
        self._on_image =  ImageTk.PhotoImage(tmpImage)

        label_on = Tkinter.Label(
            self._control_frame,
            image=self._on_image,
            borderwidth=0,
            highlightbackground=BG_COLOUR,
        )
        label_on.grid(row=0, column=0, rowspan=4)

        tmpImage = Image.open("images/labels/off.png")
        self._off_image =  ImageTk.PhotoImage(tmpImage)

        label_off = Tkinter.Label(
            self._control_frame,
            image=self._off_image,
            borderwidth=0,
            highlightbackground=BG_COLOUR,
        )
        label_off.grid(row=0, column=4, rowspan=4)

        for row in xrange(4):
            on_button = Tkinter.Button(
                self._control_frame,
                width=3,
                command=self.make_callback((row+1), "on")
            )
            on_button.grid(row=row, column=1)

            tmpImage = Image.open("images/labels/device_" + str(row + 1) + ".png")
            self._device_label_images[row] = ImageTk.PhotoImage(tmpImage)

            socket_label = Tkinter.Label(
                self._control_frame,
                image=self._device_label_images[row],
                highlightbackground=BG_COLOUR,
                borderwidth=0,
                height=35,
                background=BG_COLOUR,
            )
            socket_label.grid(row=row, column=2, pady=5)

            off_button = Tkinter.Button(
                self._control_frame,
                width=3,
                command=self.make_callback((row+1), "off")
            )
            off_button.grid(row=row, column=3)

    def get_band(self):
        return self.band.get()

class ViewAlarmsApp(Content):
    def __init__(self, parent, header_image_path, alarmKeeper, alarm_adder_app):
        Content.__init__(self, parent, header_image_path)

        self._alarmKeeper = alarmKeeper
        self._alarm_adder_app = alarm_adder_app

        self._alarm_map = dict()

        self._init_treeview()
        self._init_buttons()

    def show(self):
        self.update_tree()
        Content.show(self)

    def _init_buttons(self):
        self._button_frame = Tkinter.Frame(self, height=20, bg=BG_COLOUR)
        self._button_frame.grid(row=1, column=0)

        self._add_alarm_button = Tkinter.Button(
            self._button_frame,
            width=10,
            text="Add alarm",
            anchor=Tkinter.CENTER,
            command=self._alarm_adder_app
        )
        self._add_alarm_button.grid(pady=6, padx=6, row=1, column=0)

        self._del_alarm_button = Tkinter.Button(
            self._button_frame,
            width=10,
            text="Del alarm",
            anchor=Tkinter.CENTER,
            command=self._delete_alarm,
        )
        self._del_alarm_button.grid(pady=6, padx=6, row=1, column=1)

    def _delete_alarm(self):
        alarm_id = self._tree.focus()

        if not alarm_id:
            return

        if self._alarmKeeper.delete_alarm(self._alarm_map[alarm_id]):
            self.update_tree()

    def _init_treeview(self):
        self._tree_frame = Tkinter.Frame(self, bg=BG_COLOUR)
        self._tree_frame.grid(row=0, column=0, padx=7)

        self._tree_container = Tkinter.Frame(self._tree_frame, bg=BG_COLOUR)
        self._tree_container.grid(row=0, column=0)

        self._tree = ttk.Treeview(
            self._tree_container,
            columns=("Time", "Days"),
            show='headings',
            height=6,
            selectmode="browse",
        )

        self._vertical_scroll_bar = ttk.Scrollbar(orient=Tkinter.VERTICAL, command=self._tree.yview)
        self._tree.configure(yscroll=self._vertical_scroll_bar.set)

        self._tree.heading("#1", text="Time", anchor=Tkinter.CENTER)
        self._tree.heading("#2", text=" Days", anchor=Tkinter.W)

        self._tree.column(
            "#1", width=50, minwidth=50, anchor=Tkinter.CENTER, stretch=Tkinter.NO
        )
        self._tree.column(
            "#2", width=240, stretch=Tkinter.NO, anchor=Tkinter.W
        )

        self.update_tree()

        self._vertical_scroll_bar.grid(in_=self._tree_container, row=0, column=1, sticky='ns')
        self._tree.grid(in_=self._tree_container, row=0, column=0, sticky=Tkinter.NSEW)

    def update_tree(self):
        for alarm_view in self._tree.get_children():
            self._tree.delete(alarm_view)

        self._alarm_map = dict()

        for alarm in sorted(self._alarmKeeper.alarms):
            alarm_time = "%02d:%02d" % (
                alarm.alarm_time.hour,
                alarm.alarm_time.minute
            )
            tree_id = self._tree.insert(
                "",
                "end",
                text="Woo",
                values=(alarm_time, alarm.get_days_label()))

            self._alarm_map[tree_id] = alarm


class AlarmAdderApp(Content):
    _days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]

    def __init__(self, parent, image, alarm_keeper, alarm_set_callback):
        Content.__init__(self, parent, image)

        self._alarm_keeper = alarm_keeper
        self._alarm_set_callback = alarm_set_callback

        self._day_vars = [Tkinter.IntVar() for day in self._days]
        self._day_buttons = [None for day in self._days]
        self._day_button_unselected = [None for day in self._days]
        self._day_button_selected = [None for day in self._days]

        self._load_day_images()

        self._init_day_frame()
        self._init_detail_frame()

    def set_alarm(self):
        self._alarm_keeper.set_alarm(
            hour=int(self._hour_spinbox.get()),
            minute=int(self._min_spinbox.get()),
            days=self._get_days()
        )
        self._alarm_set_callback()

    def _get_days(self):
        active_days = []

        for i, var in enumerate(self._day_vars):
            if var.get():
                active_days.append(i)

        return active_days

    def _load_day_images(self):
        for i, day in enumerate(self._days):
            tmpImage = Image.open("images/checkbuttons/" + day + "_unselected.png")
            self._day_button_unselected[i] = ImageTk.PhotoImage(tmpImage)

            tmpImage = Image.open("images/checkbuttons/" + day + "_selected.png")
            self._day_button_selected[i] = ImageTk.PhotoImage(tmpImage)

    def _init_day_frame(self):
        self._days_frame = Tkinter.Frame(self, bg=BG_COLOUR)
        self._days_frame.grid(row=0, column=0)

        top_row = Tkinter.Frame(self._days_frame, bg=BG_COLOUR, width=300)
        top_row.grid(row=0, column=0, padx=20)

        bottom_row = Tkinter.Frame(self._days_frame, bg=BG_COLOUR)
        bottom_row.grid(row=1, column=0)

        for i, day in enumerate(self._days):
            if i < 5:
                frame = top_row
            else:
                frame = bottom_row

            self._day_buttons[i] = Tkinter.Checkbutton(
                frame,
                variable=self._day_vars[i],
                indicatoron=0,
                image=self._day_button_unselected[i],
                selectimage=self._day_button_selected[i],
                relief=Tkinter.FLAT,
                offrelief=Tkinter.FLAT,
                overrelief=Tkinter.FLAT,
                borderwidth=0,
                highlightbackground=BG_COLOUR,
            )

            if i < 5:
                self._day_buttons[i].select()
                column = i
            else:
                column = i - 5

            self._day_buttons[i].grid(row=0, column=column)

    def _init_detail_frame(self):
        self._detail_frame = Tkinter.Frame(self, bg=BG_COLOUR)
        self._detail_frame.grid(row=1, column=0)

        self._hour_spinbox = self._make_spinbox(self._detail_frame, to=23)
        self._hour_spinbox.grid(row=0, column=0)

        min_label = self._make_time_label(self._detail_frame, ":", size=30)
        min_label.grid(row=0, column=1)

        self._min_spinbox = self._make_spinbox(self._detail_frame)
        self._min_spinbox.grid(row=0, column=2)

        tmpImage = Image.open("images/checkbuttons/set_alarm.png")
        self._set_alarm_image = ImageTk.PhotoImage(tmpImage)

        self._set_button = Tkinter.Button(
            self._detail_frame,
            image=self._set_alarm_image,
            borderwidth=0,
            relief=Tkinter.FLAT,
            overrelief=Tkinter.FLAT,
            highlightbackground=BG_COLOUR,
            highlightcolor=BG_COLOUR,
            highlightthickness=-2,
            activebackground=BG_COLOUR,
            activeforeground=BG_COLOUR,
            background=BG_COLOUR,
            command=self.set_alarm,
        )
        self._set_button.grid(row=0, column=3)

    def _make_time_label(self, parent, text, size=15):
        return Tkinter.Label(
            parent,
            text=text,
            font="Mono " + str(size) + " bold",
            background=BG_COLOUR,
            foreground="#00FF00",
        )

    def _make_spinbox(self, parent, from_=0, to=59, format="%02.0f"):
        return Tkinter.Spinbox(
            self._detail_frame,
            from_=from_,
            to=to,
            width=2,
            wrap=True,
            borderwidth=0,
            insertborderwidth=0,
            selectborderwidth=0,
            format="%02.0f",
            background=BG_COLOUR,
            foreground="#00FF00",
            buttonbackground=BG_COLOUR,
            relief=Tkinter.FLAT,
            buttondownrelief=Tkinter.FLAT,
            buttonuprelief=Tkinter.FLAT,
            font="Mono 46 bold",
            highlightthickness=0,
            justify=Tkinter.CENTER,
        )

